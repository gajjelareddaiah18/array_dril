function filter(elements, cb) {
    let filteredElements = [];
    for (let index = 0; index < elements.length; index++) {
        let element = elements[index];
        let filterElement = cb(element);
        if (filterElement !== undefined) {
            filteredElements.push(filterElement);
        }
    }
    return filteredElements;
}

function cb(element) {
    if (element % 2 === 0) {
        return element;
    }
}

module.exports={
    filter,cb
}
