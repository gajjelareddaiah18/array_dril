


function reduce(elements, cb, startingValue) {
    let accumulator = startingValue !== undefined ? startingValue : elements[0];
    const startIndex = startingValue !== undefined ? 0 : 1;

    for (let index = startIndex; index < elements.length; index++) {
        let currentValue = elements[index];
        accumulator = cb(accumulator, currentValue);
    }

    return accumulator;
}

function cb(accumulator, currentValue) {
    let newValue = accumulator + currentValue;
    return newValue;
}
module.exports={
    reduce,cb
}