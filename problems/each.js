

/*
    Complete the following functions.
    These functions only need to work with arrays.
    A few of these functions mimic the behavior of the `Built` in JavaScript Array Methods.
    The idea here is to recreate the functions from scratch BUT if you'd like,
    feel free to Re-use any of your functions you build within your other functions.
    **DONT** Use for example. .forEach() to recreate each, and .map() to recreate map etc.
    You CAN use concat, push, pop, etc. but do not use the exact method that you are replicating

    Name your files like so:
        each.js
        testEach.js
        map.js
        testMap.js
*/
function each(elements,cb) {
  for(let i = 0; i < elements.length; i++){
    let element=elements[i];
    let index = i;
    cb(element,index)
    
  }
}
function cb(element,index){
    console.log(`${element},${index}`);
}

module.exports={each,cb};